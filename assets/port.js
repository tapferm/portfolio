let expandImg = document.getElementById("expandedImg");
let album = document.querySelector(".album");

function expandImage(previewImg) {
    let currentImg = previewImg;
    let imgText = document.getElementById("imgtext");
    let imgButtons = document.getElementById("imgButtons")


    expandImg.src = previewImg.src;
    imgText.innerHTML = previewImg.alt;
    album.classList.add("albumHidden");
    expandImg.parentElement.style.display = "block";
    expandImg.classList.add("expImg");
    expandImg.classList.remove("previewImg");

    imgButtons.firstElementChild.addEventListener('click', function () {
        let nextImg = currentImg.nextElementSibling;
        let firstImg = currentImg.parentElement.firstElementChild;
        if (nextImg == null) {
            expandImg.src = firstImg.src;
            imgText.innerHTML = firstImg.alt;
            currentImg = firstImg;
        } else {
            expandImg.src = nextImg.src;
            imgText.innerHTML = nextImg.alt;
            currentImg = nextImg;
        }
    });

    imgButtons.lastElementChild.addEventListener('click', function () {
        let prevImg = currentImg.previousElementSibling;
        let lastImg = currentImg.parentElement.lastElementChild;

        if (prevImg == null) {
            expandImg.src = lastImg.src;
            imgText.innerHTML = lastImg.alt;
            currentImg = lastImg;

        } else {
            expandImg.src = prevImg.src;
            imgText.innerHTML = prevImg.alt;
            currentImg = prevImg;
        }

    });

}

function closeImg(imgs) {
    imgs.parentElement.style.display = "none";
    album.classList.remove("albumHidden");
}